#include <iostream>
#include <iomanip>
using namespace std;

main(){
    const char separator    = ' ';
    const int nameWidth     = 10;
    int brs =0;
    cout << "Unesite broj studenata: ";
    cin >> brs;
    
    struct product {
  	char index[8];
  	char ime[16];
  	char prezime[16];
  	double prosjek; 
	} student[brs];
	
    for( int a = 0; a < brs; a = a + 1 ) {
         cout << "Broj indexa za student broj " << a+1 << ": ";
         cin >> student[a].index;
         cout << "Ime za student broj " << a+1 << ": ";
         cin >> student[a].ime;
         cout << "Prezime za student broj " << a+1 << ": ";
         cin >> student[a].prezime;
         cout << "Prosjek za student broj " << a+1 << ": ";
         cin >> student[a].prosjek;
         system ("pause");
    }
    cout << left << setw(nameWidth) << setfill(separator) << "Index";
    cout << left << setw(nameWidth) << setfill(separator) << "Ime";
    cout << left << setw(nameWidth) << setfill(separator) << "Prezime";
    cout << left << setw(nameWidth) << setfill(separator) << "Prosjek";
    cout << endl;
    for( int a = 0; a < brs; a = a + 1 ) {
         cout << left << setw(nameWidth) << setfill(separator) << student[a].index;
         cout << left << setw(nameWidth) << setfill(separator) << student[a].ime;
         cout << left << setw(nameWidth) << setfill(separator) << student[a].prezime;
         cout << left << setw(nameWidth) << setfill(separator) << student[a].prosjek << endl;
    }
    system("pause");          
}
